# K8s Playground

## Terraform
- Use `terraform fmt` to format the syntax avoiding warnings.
- `terraform plan -out=terraform.plan`
- `terraform apply -auto-approve -input=false terraform.plan`
