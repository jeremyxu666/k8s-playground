cfssl gencert -initca ca-csr.json \
  | cfssljson -bare ca; \
  cfssl gencert -ca=ca.pem -ca-key=ca-key.pem \
    -config=ca-config.json \
    -hostname=192.168.50.20,192.168.50.21,192.168.50.22,etcd0,etcd1,etcd2 \
    -profile=kubernetes kubernetes-csr.json \
  | cfssljson -bare kubernetes
