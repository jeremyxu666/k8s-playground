resource "aws_key_pair" "bottle" {
  key_name = "bottle"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDEiBZTwGqTMXpQQENbbdLxRYdzv8ZlmMbmTQjo8ciiG8VgYwzgA0qMfLWvi6EJRsGq/18jbY/4YgYdBEdZrs5yRsvOBZ/Rf/NvuT2RZOsOwH0Glrzs1gyGw1uhGOyX/xNw5JOJZvQPtNpfsE8dUSfMyWnruvegREPMDqlQtBK1OMtiGmjdBEo9g/hIjD8tK6sX3RbiPKRKyyVkafx6LKVI65sTEp5cTvVy9YGbLiA47DY8mnj46vxChZuAlMu45io6/QxphZXnhBSt0OAigXd3pOvBJgY5dgyIGQkkugzODnC68qQH23EwNo08htc/eQM0Ef0uzkQ1IOsFU/OGVCJ+Bx8zfIViVuK9T/a5vnchjCcqgcz00u985sFjgoQjSJhraMbfCMuJQgZaGV8zo7QAsdJhbEQzpqt8FCLR87bzXW5s5PFZvTVCKsUMq1LSEMNpmYYvrkEaXBZW/b2/5T/jXKm4TOvX/E2CysJeBGBgw/2RVX2/v16iT60yubcAb4gqaqsAejz5NJqqGHeGqG4GLDk42wmGUA66es19Q0O0a4qdpNJsSxkKX4obEzvwyCmRm7wQQf+wQMT9nyEDHFD0w137fQQF09ZsLN4mF1t9r9T7I2CTnK8neXSh0txKIfihN0Abors68UpLEjIntCczZqIIXPLXRVtYKfIdRyeCNw=="
}

resource "aws_instance" "kubernetes_controllers" {
  count=3
  ami = lookup(var.ami, var.region)
  instance_type = "t2.micro"
  key_name = aws_key_pair.bottle.key_name
  associate_public_ip_address = "true"
  iam_instance_profile= aws_iam_instance_profile.kubernetes-instance-profile.name
  vpc_security_group_ids=[aws_security_group.kubernetes-securitygroup.id]
  private_ip=cidrhost(aws_subnet.kubernetes_public_1.cidr_block, 10 + count.index)
  subnet_id=aws_subnet.kubernetes_public_1.id
  source_dest_check="false"

  tags = {
      Name = "controller-${count.index}"
      Role = "controller"
      AnsibleFilter = var.ansible_filter
      AnsibleNodeName = "controller${count.index}"
  }
}

resource "aws_instance" "kubernetes_workers" {
  count=3
  ami = lookup(var.ami, var.region)
  instance_type = "t2.micro"
  key_name = aws_key_pair.bottle.key_name
  associate_public_ip_address = "true"
  iam_instance_profile= aws_iam_instance_profile.kubernetes-instance-profile.name
  vpc_security_group_ids=[aws_security_group.kubernetes-securitygroup.id]
  private_ip=cidrhost(aws_subnet.kubernetes_public_1.cidr_block, 20 + count.index)
  subnet_id=aws_subnet.kubernetes_public_1.id
  source_dest_check="false"

  tags = {
      Name = "worker-${count.index}"
      Role = "worker"
      AnsibleFilter = var.ansible_filter
      AnsibleNodeName = "worker${count.index}"
  }
}

resource "aws_instance" "etcd_cluster_members" {
  count=3
  ami = lookup(var.ami, var.region)
  instance_type = "t2.micro"
  key_name = aws_key_pair.bottle.key_name
  associate_public_ip_address = "true"
  iam_instance_profile= aws_iam_instance_profile.kubernetes-instance-profile.name
  vpc_security_group_ids=[aws_security_group.kubernetes-securitygroup.id]
  private_ip=cidrhost(aws_subnet.kubernetes_public_1.cidr_block, 30 + count.index)
  subnet_id=aws_subnet.kubernetes_public_1.id
  source_dest_check="false"

  tags = {
      Name = "etcd-${count.index}"
      Role = "etcd"
      AnsibleFilter = var.ansible_filter
      AnsibleNodeName = "etcd${count.index}"
  }
}


output "controller_addresses" {
  description = "Private address for controller nodes"
  value = aws_instance.kubernetes_controllers.*.private_ip
}

output "worker_addresses" {
  description = "Private address for worker nodes"
  value = aws_instance.kubernetes_workers.*.private_ip
}

output "etcd_cluster_addresses" {
  description = "Private address for etcd nodes"
  value = aws_instance.etcd_cluster_members.*.private_ip
}
