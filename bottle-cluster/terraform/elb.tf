resource "aws_lb" "kubernetes_elb" {
  name               = "kubernetes-elb"
  load_balancer_type = "network"
  internal = false

  subnets = [ aws_subnet.kubernetes_public_1.id ]
}

resource "aws_lb_target_group" "kubernetes_target_group" {
  name     = "kubernetes-target-group"
  port     = 6443
  protocol = "TLS"
  target_type = "ip"
  vpc_id   = aws_vpc.kubernetes.id
}

#https://stackoverflow.com/questions/44491994/not-able-to-add-multiple-target-id-inside-targer-group-using-terraform
resource "aws_lb_target_group_attachment" "tg_attach_1" {
  target_group_arn = aws_lb_target_group.kubernetes_target_group.arn
  target_id        = "10.0.1.11"
}

resource "aws_lb_target_group_attachment" "tg_attach_2" {
  target_group_arn = aws_lb_target_group.kubernetes_target_group.arn
  target_id        = "10.0.1.12"
}

resource "aws_lb_target_group_attachment" "tg_attach_3" {
  target_group_arn = aws_lb_target_group.kubernetes_target_group.arn
  target_id        = "10.0.1.13"
}

resource "aws_alb_listener" "test-web-lb-listener" {
  load_balancer_arn = aws_lb.kubernetes_elb.arn
  port              = "443"
  protocol          = "TLS"
  certificate_arn   = "arn:aws:acm:us-east-2:675441604370:certificate/a8657fc9-a4c3-4204-b62b-325ef8437d7d"

  default_action {
    target_group_arn = aws_lb_target_group.kubernetes_target_group.arn
    type             = "forward"
  }
}

output "cluster_public_dns" {
  description = "Public DNS for cluster load balancer"
  value = aws_lb.kubernetes_elb.dns_name
}