variable "region" {
  default = "us-east-2"
}

variable "ami" {
  type = map
  default = {
    us-east-1 = "ami-0a99b06fad09f48df"
    us-east-2 = "ami-0629230e074c580f2"
  }
}

variable "ansible_filter" {
  description = "`ansibleFilter` tag value added to all instances, to enable instance filtering in Ansible dynamic inventory"
  default = "KubernetesBottle" # IF YOU CHANGE THIS YOU HAVE TO CHANGE instance_filters = tag:ansibleFilter=Kubernetes01 in ./ansible/hosts/ec2.ini
}

variable "kubernetes_cluster_dns" {
  default = "10.31.0.1"
}