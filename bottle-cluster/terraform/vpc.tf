resource "aws_vpc" "kubernetes" {
  cidr_block       = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "kubernetes"
  }
}

resource "aws_subnet" "kubernetes_public_1" {
  vpc_id     = aws_vpc.kubernetes.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone = "us-east-2a"

  tags = {
    Name = "kubernetes_public_1"
  }
}

resource "aws_internet_gateway" "kubernetes-gw" {
    vpc_id = aws_vpc.kubernetes.id

    tags = {
        Name = "kubernetes"
    }
}

resource "aws_route_table" "kubernetes-public" {
    vpc_id = aws_vpc.kubernetes.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.kubernetes-gw.id
    }

    tags = {
        Name = "kubernetes_public_1"
    }
}

resource "aws_route_table_association" "kubernetes_public_1" {
    subnet_id = aws_subnet.kubernetes_public_1.id
    route_table_id = aws_route_table.kubernetes-public.id
}
