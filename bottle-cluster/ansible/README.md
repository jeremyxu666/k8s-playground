### EC2 Dynamic Inventory
1. inventory is collecting by using `aws_ec2` plugin, instances are found and grouped
by tags.
2. `ansible-inventory -i inventory/k8s_aws_ec2.yml --list `: to get the dynamic host list for aws machines.
There are facts available by using this plugin. Like `private_dns_name`