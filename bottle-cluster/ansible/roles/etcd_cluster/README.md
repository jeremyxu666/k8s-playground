ETCDCTL_API=3 etcdctl \
  --endpoints 10.0.1.30:2379,10.0.1.31:2379,10.0.1.32:2379 \
  --cacert  /etc/etcd/ca.pem \
  --cert  /etc/etcd/kubernetes.pem \
  --key  /etc/etcd/kubernetes-key.pem \
  endpoint health


kube-apiserver \
--advertise-address=10.0.1.11 \
--allow-privileged=true \
--apiserver-count=3 \
--audit-log-maxage=30 \
--audit-log-maxbackup=3 \
--audit-log-maxsize=100 \
--audit-log-path=/var/log/audit.log \
--authorization-mode=Node,RBAC \
--bind-address=0.0.0.0 \
--client-ca-file=/var/lib/kubernetes/ca.pem \
--enable-admission-plugins=NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota \
--etcd-cafile=/var/lib/kubernetes/ca.pem \
--etcd-certfile=/var/lib/kubernetes/kubernetes.pem \
--etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \
--etcd-servers=https://10.0.1.30:2379,https://10.0.1.31:2379,https://10.0.1.32:2379 \
--event-ttl=1h \
--kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \
--kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \
--kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \
--runtime-config='api/all=true' \
--service-cluster-ip-range=10.32.0.0/24 \
--service-node-port-range=30000-32767 \
--service-account-key-file=/var/lib/kubernetes/service-account.pem \
--service-account-signing-key-file=/var/lib/kubernetes/service-account-key.pem \
--service-account-issuer=api \
--tls-cert-file=/var/lib/kubernetes/kubernetes.pem \
--tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \
--v=2


--service-account-issuer=https://kubernetes-elb-9a9fff5f5a7729a9.elb.us-east-2.amazonaws.com:443 \